export enum KataDifficulty {
    LOW = 'Low', // -> sino 0
    MEDIUM = 'Medium', // -> sino 1
    HIGH = 'High' // -> sino 2
}
