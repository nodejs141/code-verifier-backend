import express, { Express, Request, Response } from 'express';
import cors from 'cors'; // Security
import helmet from 'helmet' // Security
import routes from '../routes'; // Root Router
import swaggerUi from 'swagger-ui-express'; // Swagger
import mongoose from 'mongoose'; // ORM


// TODO HTTPS


// Create Express APP
const server: Express = express();

// Define SERVER to use "/api" and use rooRouter
server.get('/', (req: Request, res: Response) => {
    res.redirect('/api');
})

// Activate for requests to http://localhost:8000
server.use('/api', routes);

// Swagger config and route
server.use('/docs', swaggerUi.serve, swaggerUi.setup(undefined, {
    swaggerOptions: {
        url: "/swagger.json",
        explorer: true
    }
}))

// Static server
server.use(express.static('public'));

// Security Config
server.use(helmet());
server.use(cors());

// Content type config
server.use(express.urlencoded({ extended: true, limit: '50mb' }));
server.use(express.json({ limit: '50mb' }));

// Mongoose Connection
mongoose.connect('mongodb://localhost:27017/codeverifierbackend')

export default server;
