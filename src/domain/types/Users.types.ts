
export interface TUser {
    name: string,
    email: string,
    password: string,
    age: number,
    katas: string[],
    solvedKatas: {katas: string, answer: string}[]
}
