import { KataDifficulty } from "../../models/KatasDifficulty.enum"

export interface TKata {
    name: string,
    description: string,
    difficulty: KataDifficulty,
    played: number
    creator: string,
    stars: {
        value: number,
        voted: number
    },
    solution: string,
    date: Date,
}
