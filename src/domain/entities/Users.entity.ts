import mongoose, { Schema } from 'mongoose';
import { IUser } from '../interfaces/IUsers.interface';
import uniqueValidator from 'mongoose-unique-validator';

export const usersEntity = () => {

    let usersSchema = new mongoose.Schema<IUser>(
        {
            name: {type: String, required: true},
            email: {type: String, required: true, unique: true, uniqueCaseInsensitive: true},
            password: {type: String, required: true},
            age: {type: Number, min: 5, max: 90, required: true},
            katas: {type: [Schema.Types.ObjectId], ref: 'katas'},
            solvedKatas: [{ _id: false, kata: {type: Schema.Types.ObjectId, ref: 'katas'}, answer: String }]
        }
    )
    usersSchema.plugin(uniqueValidator)

    return mongoose.models.users || mongoose.model<IUser>('users', usersSchema);
}
