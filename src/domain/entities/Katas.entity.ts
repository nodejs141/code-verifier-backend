import mongoose, { Schema } from 'mongoose';
import { IKatas } from '../interfaces/IKatas.interface';
import uniqueValidator from 'mongoose-unique-validator';
import { KataDifficulty } from '../../models/KatasDifficulty.enum';

export const katasEntity = () => {

    let katasSchema = new mongoose.Schema<IKatas>(
        {
            name: {type: String, required: true, unique: true},
            description: {type: String, required: true},
            difficulty: {type: String, enum: Object.values(KataDifficulty), required: true},
            played: {type: Number, min: 0},
            creator: {type: Schema.Types.ObjectId, ref: 'users'},
            stars: {
                value: {type: Schema.Types.Decimal128, min: 0, max: 5},
                voted: {type: Number, min: 0}
            },
            solution: {type: String, required: true},
            date: {type: Date, required: true}
        }
    )
    katasSchema.plugin(uniqueValidator)

    return mongoose.models.katas || mongoose.model<IKatas>('katas', katasSchema);
}
