import { LogError } from "../../utils/logger";
import { usersEntity } from "../entities/Users.entity";
import { katasEntity } from "../entities/Katas.entity";
import { IUser } from "../interfaces/IUsers.interface";
import { IKatas } from "../interfaces/IKatas.interface";
import { Types } from "mongoose";
import { KataDifficulty } from "../../models/KatasDifficulty.enum";


/**
 * Method to obtain all users from collection "users" in Mongo Server
 */
export const getAllUsersORM = async (limit: number, page: number): Promise<any | undefined> => {
    try {
        const userModel = usersEntity();
        let totalPages: any = 0;
        let currentPage: any = 0

        // Count total documents in collection "users"
        await userModel.countDocuments().then((total: number): void => {
            totalPages = Math.ceil(total / limit) > 0 ? Math.ceil(total / limit) : 1;
            currentPage = totalPages >= page ? page : totalPages;
        });

        // Search all users (using pagination)
        const data = await userModel.find({isDelete: false})
            .limit(limit)
            .skip(totalPages >= page ? (page - 1) * limit : (totalPages - 1) * limit)
            .select({name: 1, email: 1, age: 1, katas: 1})
            .exec();
        
        return { totalPages, currentPage, users: data };

    } catch (err) {
        LogError(`[ORM ERROR]: Getting all users: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to obtain a user with your id from Mongo Server
 */
export const getUserByIdORM = async (userId: string): Promise<any | undefined> => {
    try {
        const userModel = usersEntity();

        // Search user
        return await userModel.findById(userId).select({_id: 0, name: 1, email: 1, age: 1, katas: 1});
        
    } catch (err) {
        LogError(`[ORM ERROR]: Getting user by id: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to delete a user with your id from Mongo Server
 */
export const deleteUserByIdORM = async (userId: string): Promise<any | undefined> => {
    try {
        const userModel = usersEntity();
        const kataModel = katasEntity();

        // Delete all katas of creator
        const creator = await userModel.findById(userId).exec();
        creator.katas.forEach(async (kataRef: Types.ObjectId) => {
            await kataModel.deleteOne({ _id: kataRef.toString() })
        });

        // Delete user
        return await userModel.deleteOne({ _id: userId });

    } catch (err) {
        LogError(`[ORM ERROR]: Deleting user by id: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to update a user with your id from Mongo Server
 */
export const updateUserByIdORM = async (userId: string, name?: string, email?: string, password?: string, age?: number): Promise<any | undefined> => {
    try {
        const userModel = usersEntity();
        const newUser = { name, email, password, age }

        // Update user
        return await userModel.findByIdAndUpdate(userId, newUser, { runValidators: true })

    } catch (err) {
        LogError(`[ORM ERROR]: Updating user: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to obtain all katas by user from collection "users" in Mongo Server
 */
export const getKatasByUserORM = async (userId: string, limit: number, page: number, sortKey?: any, sortOrder?: any): Promise<any | undefined> => {
    try {
        // Read both schemas
        const userModel = usersEntity();
        const kataModel = katasEntity();
        
        let totalPages: any = 0;
        let currentPage: any = 0;
        let skip: number = 0;

        // Count total documents in collection "users"
        await userModel.findById(userId).exec().then((user: IUser) => {
            const total = user.katas.length
            totalPages = Math.ceil(total / limit) > 0 ? Math.ceil(total / limit) : 1;
            currentPage = totalPages >= page ? page : totalPages;
            skip = totalPages >= page ? (page - 1) * limit : (totalPages - 1) * limit;
        })

        const data = await userModel.findById(userId)
            .select({_id: 0, email: 1, katas: 1})
            .populate({ 
                path: 'katas',
                select: { name: 1, description: 1, difficulty: 1, played: 1, stars: 1, solution: 1 },
                options: {
                    sort: { [sortKey]: sortOrder },
                    limit,
                    skip,
                }
            })
            .exec()

        return { totalPages, currentPage, user: data.name, katas: data.katas };

    } catch (err) {
        LogError(`[ORM ERROR]: Getting all katas of user: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to delete a kata with your unique id and the creator id from Mongo Server
 */
export const deleteKataByIdAndCreatorORM = async (kataId: string, creatorId: string): Promise<any | undefined> => {
    try {
        const kataModel = katasEntity();
        const userModel = usersEntity();

        const creator = await userModel.findById(creatorId).exec();
        const kata = await kataModel.findById(kataId).exec()
        if ( kata.creator.equals(creator._id) ) {
            // Deleting and update reference of creator kata
            creator.katas = creator.katas.filter((kataRef: Types.ObjectId) => {
                if (!kataRef.equals(kata._id)) {
                    return kataRef
                }
            })
            await creator.save()

            // Deleting of solved katas reference of each user
            const users = await userModel.find().exec()
            users.forEach(async (user: any) => {
                let i: number = user.solvedKatas.findIndex((obj: any) => obj.kata.equals(kata._id))
                user.solvedKatas.splice(i, 1)
                await user.save()
            })

            // Deleting kata
            return await kataModel.deleteOne({ _id: kataId });
        } else {
            throw new Error("Error id of the creator of the kata is not equal to the id of the user provided");
        }

    } catch (err) {
        LogError(`[ORM ERROR]: Deleting kata by id: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to create a kata in Mongo Server with a relationship (1 : n) between "users" and "katas"
 */
export const createKataByUserORM = async (name: string, description: string, difficulty: KataDifficulty, creator: string, solution: string): Promise<any | undefined> => {
    try {
        const kataModel = katasEntity();
        const userModel = usersEntity();

        // Check if creator id exist like user in data base
        const user = await userModel.findById(creator).exec()

        // Create kata in data base
        const kata: IKatas = {
            name,
            description,
            difficulty,
            played: 0,
            creator: user!._id,
            stars: {
                value: new Types.Decimal128('0'),
                voted: 0
            },
            solution,
            date: new Date()
        }
        const newKata = await kataModel.create(kata)

        // Add new id kata to user and update in data base
        user.katas = user.katas.concat(newKata._id)
        return await user.save()

    } catch (err) {
        LogError(`[ORM ERROR]: Creating kata: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to update a kata with the creator id from Mongo Server
 */
export const updateKataByIdAndCreatorORM = async (kataId: string, creatorId: string, name?: string, description?: string, difficulty?: string, solution?: string): Promise<any | undefined> => {
    try {
        const kataModel = katasEntity();

        const updateKata = { name, description, difficulty, solution }
        const creator = new Types.ObjectId(creatorId)
        const kata = await kataModel.findById(kataId).exec()
        if ( kata.creator.equals(creator) ) {
            return await kataModel.findByIdAndUpdate(kataId, updateKata, { runValidators: true })
        } else {
            throw new Error("Error id of the creator of the kata is not equal to the id of the user provided");
        }

    } catch (err) {
        LogError(`[ORM ERROR]: Updating kata ${kataId}: ${err}`);
        throw new Error("Error in ORM request");
    }
}
