import { katasEntity } from "../entities/Katas.entity";
import { KataDifficulty } from "../../models/KatasDifficulty.enum";
import { usersEntity } from "../entities/Users.entity";
import { LogError } from "../../utils/logger";
import { Types } from "mongoose";


/**
 * Method to obtain all katas from collection "katas" in Mongo Server
 */
export const getAllKatasORM = async (limit: number, page: number, sortKey?: any, sortOrder?: any): Promise<any | undefined> => {
    try {
        const kataModel = katasEntity();
        let totalPages: any = 0;
        let currentPage: any = 0;

        // Count total documents in collection "katas"
        await kataModel.countDocuments().then((total: number): void => {
            totalPages = Math.ceil(total / limit) > 0 ? Math.ceil(total / limit) : 1;
            currentPage = totalPages >= page ? page : totalPages;
        });

        // Search all katas (using pagination)
        const data = await kataModel.find({isDelete: false})
            .sort({[sortKey]: sortOrder})
            .limit(limit)
            .skip(totalPages >= page ? (page - 1) * limit : (totalPages - 1) * limit)
            .select({name: 1, description: 1, difficulty: 1, played: 1, creator: 1, stars: 1, solution: 1})
            .exec();

        return { totalPages, currentPage, katas: data }

    } catch (err) {
        LogError(`[ORM ERROR]: Getting all katas: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to obtain a kata with your id from Mongo Server
 */
export const getKataByIdORM = async (kataId: string): Promise<any | undefined> => {
    try {
        const kataModel = katasEntity();
        return await kataModel.findById(kataId).select({_id: 0, name: 1, description: 1, difficulty: 1, played: 1, creator: 1, stars: 1, solution: 1}).exec();
    } catch (err) {
        LogError(`[ORM ERROR]: Getting kata by id: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to delete a kata with your id from Mongo Server
 */
export const deleteKataByIdORM = async (kataId: string): Promise<any | undefined> => {
    try {
        const kataModel = katasEntity();
        const userModel = usersEntity();

        // Deleting and update reference of creator kata
        const kata = await kataModel.findById(kataId).exec();
        const creator = await userModel.findById(kata.creator.toString()).exec();
        creator.katas = creator.katas.filter((kataRef: Types.ObjectId) => {
            if (!kataRef.equals(kata._id)) {
                return kataRef
            }
        })
        await creator.save()

        // Deleting of solved katas reference of each user
        const users = await userModel.find().exec()
        users.forEach(async (user: any) => {
            let i: number = user.solvedKatas.findIndex((obj: any) => obj.kata.equals(kata._id))
            user.solvedKatas.splice(i, 1)
            await user.save()
        })

        // Deleting kata
        return await kataModel.deleteOne({ _id: kataId });

    } catch (err) {
        LogError(`[ORM ERROR]: Deleting kata by id: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to update a kata with your id from Mongo Server
 */
export const updateKataByIdORM = async (kataId: string, name?: string, description?: string, difficulty?: KataDifficulty, solution?: string): Promise<any | undefined> => {
    try {
        const kataModel = katasEntity();
        const updateKata = { name, description, difficulty, solution }

        return await kataModel.findByIdAndUpdate(kataId, updateKata, { runValidators: true })
    } catch (err) {
        LogError(`[ORM ERROR]: Updating kata ${kataId}: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to add kata stars with your id from Mongo Server
 */
export const addKataStarsORM = async (kataId: string, stars: number): Promise<any | undefined> => {
    try {
        const kataModel = katasEntity();
        let kata: any
        await kataModel.findById(kataId, (err: any, docs: any) => {
            let newStars: number = (Number(docs.stars.value) * docs.stars.voted / (docs.stars.voted + 1)) + (stars / (docs.stars.voted + 1))
            if ( !isNaN(newStars) ) {
                kata = {
                    stars: {
                        value: Number(newStars.toFixed(2)),
                        voted: (docs.stars.voted + 1)
                    }
                }
            } else {
                throw new Error("ORM operation not number");
            }
        }).clone()
        return await kataModel.findByIdAndUpdate(kataId, kata)
    } catch (err) {
        LogError(`[ORM ERROR]: Adding stars kata: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to add kata stars with your id from Mongo Server
 */
export const solveKataByIdORM = async (userId: string, kataId: string, answer: string): Promise<any | undefined> => {
    try {
        const kataModel = katasEntity();
        const userModel = usersEntity();
        
        // Find user and kata elements
        const user = await userModel.findById(userId).exec();
        const kata = await kataModel.findById(kataId).exec();
        let isExist = false;

        // Check if the solution exists
        user.solvedKatas.forEach((obj: any) => {
            if ( kata._id.equals(obj.kata) ) {
                obj.answer = answer
                isExist = true
            }
        });

        // Register answer and kata id in user
        if ( !isExist ) { user.solvedKatas = user.solvedKatas.concat({kata: kata._id, answer}) }
        await user.save();
        
        // Add played to kata
        kata.played = kata.played + 1
        await kata.save();

        return kata.solution

    } catch (err) {
        LogError(`[ORM ERROR]: Adding kata solve answer: ${err}`);
        throw new Error("Error in ORM request");
    }
}
