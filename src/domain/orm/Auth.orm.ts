import { usersEntity } from "../entities/Users.entity";
import { IUser } from "../interfaces/IUsers.interface";
import { LogError } from "../../utils/logger";
import { IAuth } from "../interfaces/IAuth.interface";
import dotenv from 'dotenv';
import jwt from 'jsonwebtoken'; // Token JWT
import bcrypt from 'bcrypt'; // Manage password


// Configuration of environment variables
dotenv.config()

// Obtain secret key to generate JWT
const secret = process.env.SECRETKEY || 'MYSECRETKEY'

/**
 * Method to register the a user in Mongo Server
 */
export const registerUserORM = async (user: IUser): Promise<any | undefined> => {
    try {
        const userModel = usersEntity();

        // Register user
        return await userModel.create(user);

    } catch (err) {
        LogError(`[ORM ERROR]: Registering user ${user.name} error: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to auth the login with a user id from Mongo Server
 */
export const loginUserORM = async (auth: IAuth): Promise<any | undefined> => {
    try {
        const userModel = usersEntity();

        let userFound: IUser | undefined = undefined
        let token: string | undefined = undefined

        // Check if user exists by email
        await userModel.findOne({ email: auth.email })
            .then((user: IUser) => {
                userFound = user;
            })
            .catch((error) => {
                LogError(`[ERROR Auth in ORM]: User not found`)
                throw new Error(`[ERROR Auth in ORM]: User not found: ${error}`)
            });
        
        // Check if password is Valid (Compare with bcrypt)
        let isValidPassword = bcrypt.compareSync(auth.password, userFound!.password);
        if (!isValidPassword) {
            LogError(`[ERROR Auth in ORM]: Password not valid`)
            throw new Error(`[ERROR Auth in ORM]: Password not valid`)
        }

        // Create JWT
        token = jwt.sign({email: userFound!.email}, secret, {
            expiresIn: "2h"
        })

        return {
            user: userFound,
            token
        }

    } catch (err) {
        LogError(`[ORM ERROR]: Login user error: ${err}`);
        throw new Error("Error in ORM request");
    }
}

/**
 * Method to logout an user
 */
export const logoutUserORM = async (): Promise<any | undefined> => {
    try {
        const userModel = usersEntity();

        // TODO: logout user

    } catch (err) {
        LogError(`[ORM ERROR]: Logout user error: ${err}`);
    }
}
