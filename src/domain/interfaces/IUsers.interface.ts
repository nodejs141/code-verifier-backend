import { Types } from 'mongoose';

export interface IUser {
    name: string,
    email: string,
    password: string,
    age: number,
    katas: Types.ObjectId[],
    solvedKatas: {katas: Types.ObjectId, answer: string}[]
}
