import { Types } from 'mongoose';
import { KataDifficulty } from '../../models/KatasDifficulty.enum';

export interface IKatas {
    name: string,
    description: string,
    difficulty: KataDifficulty,
    played: number
    creator: Types.ObjectId,
    stars: {
        value: Types.Decimal128,
        voted: number
    },
    solution: string,
    date: Date,
}
