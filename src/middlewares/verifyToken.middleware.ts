import jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';
import dotenv from 'dotenv';

// Configuration of environment variables
dotenv.config()

// Obtain secret key to generate JWT
const secret = process.env.SECRETKEY || 'MYSECRETKEY'


/**
 * 
 * @param {Request} req Original request previous middleware of verification JWt
 * @param {Response} res Response to verification of JWt
 * @param {NextFunction} next Next function to be executed
 * @returns Error of verification or next execution
 */
export const verifyToken = (req: Request, res: Response, next: NextFunction) => {

    // Check Header frm Request for 'x-access-token'
    const token: any = req.headers['x-access-token'];

    // Verify if jwtToken is present
    if (!token) {
        return res.status(403).send({
            authenticationError: 'Missing JWT in request',
            message: 'Not authorised to consume this endpoint'
        });
    }

    // Verify the jwtToken obtain. We pass the secret
    jwt.verify(token, secret, (err: any, decoded: any) => {

        if (err) {
            return res.status(500).send({
                authenticationError: 'JWT verification failed',
                message: 'Failed to verify JWT Token in request'
            });
        }

        // Pass something to next request (id of user || other info)
        // Execute Next Function -> Protected Routes will be executed 
        next();
    })
}
