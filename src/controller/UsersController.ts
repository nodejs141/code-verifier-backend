import { BodyProp, Delete, Get, Post, Put, Query, Route, Security, Tags} from 'tsoa'
import { LogSuccess } from "../utils/logger";
import { IUsersController } from './interfaces';
import { createKataByUserORM, deleteKataByIdAndCreatorORM, deleteUserByIdORM, getAllUsersORM, getKatasByUserORM, getUserByIdORM, updateKataByIdAndCreatorORM, updateUserByIdORM } from '../domain/orm/Users.orm'; // Users ORM
import { KataDifficulty } from '../models/KatasDifficulty.enum';


@Route("/api/users")
@Tags("UsersController")
export class UsersController implements IUsersController {

    /**
     * Endpoint to retrieve all Users with pagination or a single user according to the id from the collection "users" of database (Require JWT)
     * @param {number} limit maximum number of users to retrieve per page
     * @param {number} page page number of the total pages to retrieve
     * @param {string} userId single user id to retrieve (optional)
     * @returns All users with pagination or user by id
     */
    @Security("ApiKeyAuth")
    @Get("/")
    public async getUser(@Query() limit: number, @Query() page: number, @Query() userId?: string): Promise<any> {
        try {
            let response: any = '';
    
            if ( userId ) {
                LogSuccess(`[/api/users] Get user by id:${userId}`)
                response = await getUserByIdORM(userId);
            } else {
                LogSuccess('[/api/users] Get all users request')
                response = await getAllUsersORM(limit, page);
            }
            
            return response
            
        } catch (err) {
            throw new Error(`${err}`);
        }
    }

    /**
     * Endpoint to delete the Users in the collection "users" of database (Require JWT)
     * @param {string} userId id of user to delete
     * @returns Message informing if deletion was correct
     */
    @Security("ApiKeyAuth")
    @Delete("/")
    public async deleteUser(@Query() userId: string): Promise<any> {
        try {
            LogSuccess('[/api/users] Delete user by request')
    
            await deleteUserByIdORM(userId)
            const response = {
                message: `User with id:${userId} deleted successfully`
            }
            return response;
            
        } catch (err) {
            throw new Error(`${err}`);
        }       
    }

    /**
     * Endpoint to update the Users in the collection "users" of database (Require JWT)
     * @param {string} userId id of user to update
     * @param {string} name new name of user to update (Optional)
     * @param {string} email new email of user to update (Optional)
     * @param {string} password new password of user to update (Optional)
     * @param {number} age new age of user (min = 5 and max = 90) to update (Optional)
     * @returns Message informing if updating was correct
     */
    @Security("ApiKeyAuth")
    @Put("/")
    public async updateUserById(@BodyProp() userId: string, @BodyProp() name?: string, @BodyProp() email?: string, @BodyProp() password?: string, @BodyProp() age?: number): Promise<any> {
        try {
            LogSuccess(`[/api/users] Update user with id:${userId}`)
    
            await updateUserByIdORM(userId, name, email, password, age)
            const response = {
                message: `User with id:${userId} updated successfully`
            }
            return response;
            
        } catch (err) {
            throw new Error(`${err}`);
        }
    }

    /**
     * Endpoint to retrieve all katas with pagination of a user by id (Require JWT)
     * @param {string} userId id of user creator
     * @param {number} limit maximum number of katas to retrieve per page
     * @param {number} page page number of the total pages to retrieve
     * @param {string} sortKey key in text of the value of the field by which you want to organize the katas (optional)
     * @param {number} sortOrder value that specifies whether the order is ascending = 1 or descending = -1 (optional)
     * @returns All kata created by a user with pagination
     */
    @Security("ApiKeyAuth")
    @Get("/katas")
    public async getKatasByUser(@Query() userId: string, @Query() limit: number, @Query() page: number, @Query() sortKey?: string, @Query() sortOrder?: number): Promise<any> {
        try {
            LogSuccess(`[/api/users/katas] Get katas of user with id: ${userId}`)
            return await getKatasByUserORM(userId, limit, page, sortKey, sortOrder);
            
        } catch (err) {
            throw new Error(`${err}`);
        }
    }

    /**
     * Endpoint to remove a Kata in the "katas" collection from the database only if the creator id is provided (Require JWT)
     * @param {string} kataId id of kata to delete
     * @param {string} creatorId id of creator of kata
     * @returns Message informing if deletion was correct
     */
    @Security("ApiKeyAuth")
    @Delete("/katas")
    public async deleteKataByIdAndCreator(@Query() kataId: string, @Query() creatorId: string): Promise<any> {
                
        try {
            LogSuccess('[/api/users/katas] Delete kata by request')

            await deleteKataByIdAndCreatorORM(kataId, creatorId)
            const response = {
                message: `Kata with id:${kataId} and creator id:${creatorId} deleted successfully`
            }
            return response;

        } catch (err) {
            throw new Error(`${err}`);
        }
    }

    /**
     * Endpoint to create the Katas in the collection "katas" of database (Require JWT)
     * @param {string} name name of kata to create
     * @param {string} description description of kata to create
     * @param {KataDifficulty} difficulty difficulty of kata to create (Basic, Medium, High)
     * @param {string} creator cretor id of kata to create
     * @param {string} solution solution of kata to create
     * @returns Message informing if creting was correct
     */
    @Security("ApiKeyAuth")
    @Post("/katas")
    public async createKataByUser(@BodyProp() name: string, @BodyProp() description: string, @BodyProp() difficulty: KataDifficulty, @BodyProp() creator: string, @BodyProp() solution: string): Promise<any> {      
        try {
            LogSuccess(`[/api/users/katas] Create kata with name: ${name}`)
            
            await createKataByUserORM(name, description, difficulty, creator, solution)
            const response = {
                message: `Kata with name:${name} created successfully`
            }
            return response;
            
        } catch (err) {
            throw new Error(`${err}`);
        }
    }

    /**
     * Endpoint to update the Katas by id in the "katas" collection of the database with a specified creator id (Require JWT)
     * @param {string} kataId id of kata to update
     * @param {string} creatorId id of creator of kata
     * @param {string} name new name of kata to update (optional)
     * @param {string} description new description of kata to update (optional)
     * @param {KataDifficulty} difficulty new difficulty of kata (Basic, Medium, High) to update (optional)
     * @param {string} solution new solution of kata to update (optional)
     * @returns Message informing if updating was correct
     */
    @Security("ApiKeyAuth")
    @Put("/katas")
    public async updateKataByIdAndCreator(@BodyProp() kataId: string, @BodyProp() creatorId: string, @BodyProp() name?: string, @BodyProp() description?: string, @BodyProp() difficulty?: KataDifficulty, @BodyProp() solution?: string): Promise<any> {
        try {
            LogSuccess(`[/api/users/katas] Update kata with id:${kataId} and cretor id:${creatorId}`)

            await updateKataByIdAndCreatorORM(kataId, creatorId, name, description, difficulty, solution)
            const response = {
                message: `Kata with id:${kataId} and cretor id:${creatorId} updated successfully`
            }
            return response;       
            
        } catch (err) {
            throw new Error(`${err}`);
        }
    }

}
