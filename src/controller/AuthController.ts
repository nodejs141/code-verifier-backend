import { Post, Route, Tags, Body } from 'tsoa'
import { IAuth } from '../domain/interfaces/IAuth.interface';
import { IUser } from '../domain/interfaces/IUsers.interface';
import { LogSuccess } from "../utils/logger";
import { IAuthController } from './interfaces';
import { loginUserORM, registerUserORM } from '../domain/orm/Auth.orm';
import { AuthResponse, ErrorResponse } from './types';


@Route("/api/auth")
@Tags("AuthController")
export class AuthController implements IAuthController {

    /**
     * Endpoint to create new users in the collection "users" of data base
     * @param {IUser} user Object of new user entity
     * @returns Message informing if create was correct
     */
    @Post("/register")
    public async registerUser(@Body() user: IUser): Promise<any> {
        try {
            LogSuccess(`[/api/auth/register] Register new user: ${user.name}`)
            
            await registerUserORM(user)
            const response = {
                message: `User created successfully: ${user.name}`
            }
            return response;

        } catch (err) {
            throw new Error(`${err}`);
        }
    }

    /**
     * Endpoint to login and be able to take action in app
     * @param {IAuth} auth object with the email and password of a registered user
     * @returns Message with welcome greeting and temporary access token (JWT)
     */
    @Post("/login")
    public async loginUser(@Body() auth: IAuth): Promise<any> {
        try {
            let response: AuthResponse | ErrorResponse | undefined;
            LogSuccess(`[/api/auth/login] Login user: ${auth.email}`)
            
            const reqPostDB = await loginUserORM(auth)
            response = {
                message: `Welcome, ${reqPostDB.user.name}`,
                token: reqPostDB.token // JWT generated for logged in user
            }
            return response;

        } catch (err) {
            throw new Error(`${err}`);
        }
    }

    /**
     * Description
     * @returns return
     */
    @Post("/logout")
    public async logoutUser(): Promise<any> {
        
        let response: any = '';

        // TODO: Close Session of user
        throw new Error("Method not implement");
    }
}
