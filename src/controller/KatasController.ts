import { BodyProp, Delete, Get, Put, Query, Route, Security, Tags} from 'tsoa'
import { LogSuccess } from "../utils/logger";
import { IKatasController } from './interfaces';
import { addKataStarsORM, deleteKataByIdORM, getAllKatasORM, getKataByIdORM, solveKataByIdORM, updateKataByIdORM } from '../domain/orm/Katas.orm';
import { KataDifficulty } from '../models/KatasDifficulty.enum';


@Route("/api/katas")
@Tags("KatasController")
export class KatasController implements IKatasController {

    /**
     * Endpoint to retrieve the Katas in the collection "katas" of database (Require JWT)
     * @param {number} limit maximum number of katas to retrieve per page
     * @param {number} page page number of the total pages to retrieve
     * @param {string} sortKey key in text of the value of the field by which you want to organize the katas (optional)
     * @param {number} sortOrder value that specifies whether the order is ascending = 1 or descending = -1 (optional)
     * @param {string} kataId single kata id to retrieve (optional)
     * @returns All katas with pagination or kata by id
     */
    @Security("ApiKeyAuth")
    @Get("/")
    public async getKata(@Query() limit: number, @Query() page: number, @Query() sortKey?: string, @Query() sortOrder?: number, @Query() kataId?: string): Promise<any> {
        try {
            let response: any = '';

            if ( kataId ) {
                LogSuccess(`[/api/katas] Get kata by id:${kataId} request`)
                response = await getKataByIdORM(kataId);
            } else {
                LogSuccess('[/api/katas] Get all katas request')
                response = await getAllKatasORM(limit, page, sortKey, sortOrder);
            }
            
            return (response)

        } catch (err) {
            throw new Error(`${err}`);
        }
    }

    /**
     * Endpoint to delete the Katas in the collection "katas" of database (Require JWT)
     * @param {string} kataId id of kata to delete
     * @returns Message informing if deletion was correct
     */
    @Security("ApiKeyAuth")
    @Delete("/")
    public async deleteKata(@Query() kataId: string): Promise<any> {
                
        try {
            LogSuccess('[/api/katas] Delete kata by request')

            await deleteKataByIdORM(kataId)
            const response = {
                message: `Kata with id:${kataId} deleted successfully`
            }
            return response;

        } catch (err) {
            throw new Error(`${err}`);
        }
    }

    /**
     * Endpoint to update to the Katas in the collection "katas" of database (Require JWT)
     * @param {string} kataId id of kata to update
     * @param {string} name new name of kata to update (optional)
     * @param {string} description new description of kata to update (optional)
     * @param {KataDifficulty} difficulty new difficulty of kata (Basic, Medium, High) to update (optional)
     * @param {string} solution new solution of kata to update (optional)
     * @returns Message informing if updating was correct
     */
    @Security("ApiKeyAuth")
    @Put("/")
    public async updateKata(@BodyProp() kataId: string, @BodyProp() name?: string, @BodyProp() description?: string, @BodyProp() difficulty?: KataDifficulty, @BodyProp() solution?: string): Promise<any> {
        try {
            LogSuccess(`[/api/katas] Update kata with id: ${kataId}`)

            await updateKataByIdORM(kataId, name, description, difficulty, solution)
            const response = {
                message: `Kata with id:${kataId} updated successfully`
            }
            return response;       
            
        } catch (err) {
            throw new Error(`${err}`);
        }
    }

    /**
     * Endpoint to add star rating to the Katas in the collection "katas" of database (Require JWT)
     * @param {string} kataId id of kata to update
     * @param {number} stars stars sent to rank a kata (min = 0 and max = 5)
     * @returns Message informing if updating score was correct
     */
    @Security("ApiKeyAuth")
    @Put("/stars")
    public async updateStarsKata(@BodyProp() kataId: string, @BodyProp() stars: number): Promise<any> {
        try {
            LogSuccess(`[/api/katas/stars] Add stars ${stars} kata with id:${kataId}`)
    
            await addKataStarsORM(kataId, stars)
            const response = {
                message: `Kata with id:${kataId} added score ${stars} successfully`
            }
            return response;
            
        } catch (err) {
            throw new Error(`${err}`);
        }
    }

    /**
     * Final point to add the solution to a Kata and register it in the list of resolved katas (Require JWT)
     * @param {string} userId id of the user who wants to register the solution
     * @param {string} kataId kata to be solved by the user
     * @param {string} answer answer to the problem raised in the kata
     * @returns Message informing the registration of the solution and sending the solution proposed by the creator of the kata
     */
    @Security("ApiKeyAuth")
    @Put("/solve")
    public async solveKataById(@BodyProp() userId: string, @BodyProp() kataId: string, @BodyProp() answer: string): Promise<any> {
        try {
            LogSuccess(`[/api/katas/solve] Add answer ${answer} of kata with id:${kataId}`)
    
            const reqDB = await solveKataByIdORM(userId, kataId, answer)
            const response = {
                message: `The solution to the kata with id:${kataId} has been registered successfully`,
                CreatorSolution: reqDB
            }
            return response;
            
        } catch (err) {
            throw new Error(`${err}`);
        }
    }
}

