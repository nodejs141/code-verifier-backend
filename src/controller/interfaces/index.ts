import { IAuth } from "../../domain/interfaces/IAuth.interface";
import { IUser } from "../../domain/interfaces/IUsers.interface";
import { KataDifficulty } from "../../models/KatasDifficulty.enum";
import { BasicResponse } from "../types";

export interface IHelloController {
    getMessage(name?: string): Promise<BasicResponse>
}

export interface IUsersController {
    // Read users from data base
    getUser(limit: number, page: number, userId?: string): Promise<any>
    
    // Delete users from data base
    deleteUser(userId: string): Promise<any>
    
    // Update users from data base
    updateUserById(userId: string, name?: string, email?: string, password?: string, age?: number): Promise<any>

    // Read katas by user from data base
    getKatasByUser(userId: string, limit: number, page: number): Promise<any>

    // Delete kata by id and creator id from data base
    deleteKataByIdAndCreator(kataId: string, creatorId: string): Promise<any>

    // Create kata by user from data base
    createKataByUser(name: string, description: string, difficulty: string, creator: string, solution: string): Promise<any>
    
    // Update kata by id and creator id from data base
    updateKataByIdAndCreator(kataId: string, creatorId: string, name?: string, description?: string, difficulty?: KataDifficulty, solution?: string): Promise<any>
}

export interface IAuthController {
    // Register user in data base
    registerUser(user: IUser): Promise<any>
    
    // Login user
    loginUser(auth: IAuth): Promise<any>
    
    // Logout user
    logoutUser(): Promise<any>
}

export interface IKatasController {
    getKata(limit: number, page: number, sortKey?: string, sortOrder?: number, kataId?: string): Promise<any>
    deleteKata(kataId: string): Promise<any>
    updateKata(kataId: string, name?: string, description?: string, difficulty?: KataDifficulty, solution?: string): Promise<any>
    updateStarsKata(kataId: string, stars: number): Promise<any>
    solveKataById(userId: string, kataId: string, answer: string): Promise<any>
}
