import { UsersController } from '../controller/UsersController';
import express, { Request, Response } from 'express';
import { LogInfo } from '../utils/logger';
import bcrypt from 'bcrypt'
import { verifyToken } from '../middlewares/verifyToken.middleware'; // Middleware Authentication
import { KataDifficulty } from '../models/KatasDifficulty.enum';


// Router from express
let usersRouter = express.Router();

// http://localhost:8000/api/users/
usersRouter.route('/')
    // GET
    .get(verifyToken, async (req: Request, res: Response) => {
        try {
            // Obtain query param
            const limit: any = req?.query?.limit || 20;
            const page: any = req?.query?.page || 1;
            const userId: any = req?.query?.userId;
            LogInfo(`Query param: ${limit}, ${page}, ${userId}`);

            // Controller Instance to execute method
            const controller: UsersController = new UsersController;
    
            // Obtain Response
            const response: any = await controller.getUser(limit, page, userId);
    
            // Send to the client the response
            return res.status(200).send(response);
            
        } catch (err) {
            return res.status(400).send({
                message: 'GET ERROR: Some of the data in get request is invalid'
            });
        }
    })
    // DELETE
    .delete(verifyToken, async (req: Request, res: Response) => {
        try {
            // Obtain query param
            const userId: any = req?.query?.userId;
            LogInfo(`Query param: ${userId}`);

            if ( userId ) {
                // Controller Instance to execute method
                const controller: UsersController = new UsersController;
        
                // Obtain Response
                const response: any = await controller.deleteUser(userId);
        
                // Send to the client the response
                return res.status(200).send(response);
                
            } else {
                return res.status(400).send({
                    message: 'MISSING DATA: Please, provide an id to remove an user from data base'
                });
            }
            
        } catch (err) {
            return res.status(400).send({
                message: 'DELETE ERROR: Some of the data in delete request is invalid'
            });
        }
    })
    // UPDATE
    .put(verifyToken, async (req: Request, res: Response) => {
        try {
            // Obtain body param
            const userId: string | undefined = req?.body?.userId;
            const name: string | undefined = req?.body?.name;
            const email: string | undefined = req?.body?.email;
            const password: string | undefined = req?.body?.password;
            const age: number | undefined = req?.body?.age;
            let hashedPassword: string | undefined = undefined;
            LogInfo(`Body param: ${userId}, ${name}, ${email}, ${age}`);

            if ( userId && ( name || email || password || age ) ) {
    
                // Obtain the password in request body and cypher
                if ( password ) {
                    hashedPassword = bcrypt.hashSync(password, 8);
                }

                // Controller Instance to execute method
                const controller: UsersController = new UsersController;
        
                // Obtain Response
                const response: any = await controller.updateUserById(userId, name, email, hashedPassword, age);
        
                // Send to the client the response
                return res.status(200).send(response);
    
            } else {
                // Send to the client the response error
                return res.status(400).send({
                    message: 'MISSING DATA: Please, provide an id and some valid property to update an existing kata from data base'
                });
            }
            
        } catch (err) {
            return res.status(400).send({
                message: 'UPDATE ERROR: Some of the data in update request is invalid'
            });
        }
    })


// http://localhost:8000/api/users/katas
usersRouter.route('/katas')
    // GET KATAS BY USER
    .get(verifyToken, async (req: Request, res: Response) => {
        try {
            // Obtain query param
            const userId: any = req?.query?.userId;
            const limit: any = req?.query?.limit || 20;
            const page: any = req?.query?.page || 1;
            const sortKey: any = req?.query?.sortKey;
            const sortOrder: any = req?.query?.sortOrder;
            LogInfo(`Query param: ${userId}, ${limit}, ${page}, ${sortKey}, ${sortOrder}`);

            if ( userId && limit && page ) {
                // Controller Instance to execute method
                const controller: UsersController = new UsersController;
        
                // Obtain Response
                const response: any = await controller.getKatasByUser(userId, limit, page, sortKey, sortOrder);
        
                // Send to the client the response
                return res.status(200).send(response);
                
            } else {
                return res.status(400).send({
                    message: 'MISSING DATA: Please, provide an userId to get request'
                });
            }
            
        } catch (err) {
            return res.status(400).send({
                message: 'GET ERROR: Some of the data in get request is invalid'
            });
        }
    })
    // DELETE KATA BY ID AND CREATOR ID
    .delete(verifyToken, async (req: Request, res: Response) => {
        try {
            // Obtain query param
            let kataId: any = req?.query?.kataId;
            let creatorId: any = req?.query?.creatorId;
            LogInfo(`Query param: ${kataId}, ${creatorId}`);

            if ( kataId && creatorId ) {
                // Manage Response
                const controller: UsersController = new UsersController;
                const response: any = await controller.deleteKataByIdAndCreator(kataId, creatorId);
                return res.status(200).send(response);
    
            } else {
                return res.status(400).send({
                    message: 'MISSING DATA: Please, provide an kata id and creator id to remove a kata from data base'
                });
            }

        } catch (err) {
            return res.status(400).send({
                message: 'DELETE ERROR: Some of the data in delete request is invalid'
            });
        }
    })
    // CREATE KATAS BY USER
    .post(verifyToken, async (req: Request, res: Response) => {
        try {
            // Obtain body param
            let name: string = req?.body?.name
            let description: string = req?.body?.description
            let difficulty: KataDifficulty = req?.body?.difficulty
            let creator: string = req?.body?.creator
            let solution: string = req?.body?.solution
            LogInfo(`Body param: ${name}, ${description}, ${difficulty}, ${creator}, ${solution}`);

            if ( name && description && difficulty && creator && solution ) {
                
                // Manage Response
                const controller: UsersController = new UsersController;
                const response: any = await controller.createKataByUser(name, description, difficulty, creator, solution);
                return res.status(201).send(response);
                
            } else {
                return res.status(400).send({
                    message: 'MISSING DATA: Please, provide an valid kata entity to create in data base'
                });
            }
            
        } catch (err) {
            return res.status(400).send({
                message: 'CREATE ERROR: Some of the data in create request is invalid'
            });
        }
    })
    // UPDATE KATA BY ID AND CREATOR ID
    .put(verifyToken, async (req: Request, res: Response) => {
        try {
            // Obtain body param
            const kataId: string | undefined = req?.body?.kataId
            const creatorId: string | undefined = req?.body?.creatorId
            const name: string | undefined = req?.body?.name
            const description: string | undefined = req?.body?.description
            const difficulty: KataDifficulty | undefined = req?.body?.difficulty
            const solution: string | undefined = req?.body?.solution
            LogInfo(`Body param: ${kataId}, ${creatorId}, ${name}, ${description}, ${difficulty}, ${solution}`);

            if ( kataId && creatorId && (name || description || difficulty || solution) ) {
                
                // Manage Response
                const controller: UsersController = new UsersController;
                const response: any = await controller.updateKataByIdAndCreator(kataId, creatorId, name, description, difficulty, solution);
                return res.status(200).send(response);
    
            } else {
                return res.status(400).send({
                    message: 'MISSING DATA: Please, provide an kata id, creator id and some valid property to update an existing kata from data base'
                });
            }
            
        } catch (err) {
            return res.status(400).send({
                message: 'UPDATE ERROR: Some of the data in update request is invalid'
            });
        }
    })


export default usersRouter;

// Get Documents => 200 ok
// Create Documents => 201 ok
// Delete Documents => 200 (Entity) or 204 (No return)
// Update Documents => 200 (Entity) or 204 (No return)
