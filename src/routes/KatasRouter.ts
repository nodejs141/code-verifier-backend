import { KatasController } from '../controller/KatasController';
import express, { Request, Response } from 'express'
import { LogInfo } from '../utils/logger';
import { KataDifficulty } from '../models/KatasDifficulty.enum';
import { verifyToken } from '../middlewares/verifyToken.middleware'; // Middleware Authentication


// Router from express
let katasRouter = express.Router();

// http://localhost:8000/api/katas/
katasRouter.route('/')
    // GET
    .get(verifyToken, async (req: Request, res: Response) => {
        try {
            // Obtain query param
            const limit: any = req?.query?.limit || 20;
            const page: any = req?.query?.page || 1;
            const sortKey: any = req?.query?.sortKey;
            const sortOrder: any = req?.query?.sortOrder;
            const kataId: any = req?.query?.kataId;
            LogInfo(`Query param: ${limit}, ${page}, ${sortKey}, ${sortOrder}, ${kataId}`);
            
            // Manage Response
            const controller: KatasController = new KatasController;
            const response: any = await controller.getKata(limit, page, sortKey, sortOrder, kataId);
            return res.status(200).send(response);
            
        } catch (err) {
            return res.status(400).send({
                message: 'GET ERROR: Some of the data in create request is invalid'
            });
        }
    })
    // DELETE
    .delete(verifyToken, async (req: Request, res: Response) => {
        try {
            // Obtain query param
            const kataId: any = req?.query?.kataId;
            LogInfo(`Query param: ${kataId}`);

            if ( kataId ) {
                // Manage Response
                const controller: KatasController = new KatasController;
                const response: any = await controller.deleteKata(kataId);
                return res.status(200).send(response);
    
            } else {
                return res.status(400).send({
                    message: 'MISSING DATA: Please, provide an id to remove an kata from database'
                });
            }

        } catch (err) {
            return res.status(400).send({
                message: 'DELETE ERROR: Some of the data in delete request is invalid'
            });
        }
    })
    // UPDATE
    .put(verifyToken, async (req: Request, res: Response) => {
        try {
            // Obtain body param
            const kataId: string | undefined = req?.body?.kataId
            const name: string | undefined = req?.body?.name
            const description: string | undefined = req?.body?.description
            const difficulty: KataDifficulty | undefined = req?.body?.difficulty
            const solution: string | undefined = req?.body?.solution
            LogInfo(`Body param: ${kataId}, ${name}, ${description}, ${difficulty}, ${solution}`);

            if ( kataId && (name || description || difficulty || solution) ) {
                
                // Manage Response
                const controller: KatasController = new KatasController;
                const response: any = await controller.updateKata(kataId, name, description, difficulty, solution);
                return res.status(200).send(response);
    
            } else {
                return res.status(400).send({
                    message: 'MISSING DATA: Please, provide an id and some valid property to update an existing kata from database'
                });
            }
            
        } catch (err) {
            return res.status(400).send({
                message: 'UPDATE ERROR: Some of the data in update request is invalid'
            });
        }
    })


// http://localhost:8000/api/katas/stars
katasRouter.route('/stars')
    // UPDATE KATA STARS
    .put(verifyToken, async (req: Request, res: Response) => {
        try {
            // Obtain body param
            const kataId: string = req?.body?.kataId
            const stars: number = req?.body?.stars
            LogInfo(`Body param: ${kataId}, ${stars}`);
            
            if ( kataId && (( stars >= 0 ) && ( stars <= 5 )) ) {

                // Manage Response
                const controller: KatasController = new KatasController;
                const response: any = await controller.updateStarsKata(kataId, stars);
                return res.status(200).send(response);
    
            } else {
                return res.status(400).send({
                    message: 'MISSING DATA: Please, provide an id and valid score to update an existing kata from database'
                });
            }
            
        } catch (err) {
            return res.status(400).send({
                message: 'UPDATE ERROR: Some of the data in update stars score request is invalid'
            });
        }
    })


// http://localhost:8000/api/katas/solve
katasRouter.route('/solve')
    // RESOLVE USER ANY KATA BY ID
    .put(verifyToken, async (req: Request, res: Response) => {
        try {
            // Obtain body param
            const userId: string | undefined = req?.body?.userId
            const kataId: string | undefined = req?.body?.kataId
            const answer: string | undefined = req?.body?.answer
            LogInfo(`Body param: ${userId}, ${kataId}, ${answer}`);
            
            if ( userId && kataId && answer ) {

                // Manage Response
                const controller: KatasController = new KatasController;
                const response: any = await controller.solveKataById(userId, kataId, answer);
                return res.status(200).send(response);
    
            } else {
                return res.status(400).send({
                    message: 'MISSING DATA: Please, provide an user id, kata id and valid answer to add on your solved katas in database'
                });
            }
            
        } catch (err) {
            return res.status(400).send({
                message: 'UPDATE ERROR: Some of the data in register answer request is invalid'
            });
        }
    })


export default katasRouter;
