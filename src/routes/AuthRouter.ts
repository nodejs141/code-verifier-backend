import express, { Request, Response } from 'express';
import { AuthController } from '../controller/AuthController';
import { IUser } from '../domain/interfaces/IUsers.interface';
import { IAuth } from '../domain/interfaces/IAuth.interface';
import bcrypt from 'bcrypt'; // BCRYPT for passwords


// Router from express
let authRouter = express.Router();

// http://localhost:8000/api/auth/register
authRouter.route('/register')
    .post(async (req: Request, res: Response) => {
        try {
            // Obtain data of user in request body
            const { name, email, password, age } = req?.body;
            let hashedPassword: string = '';

            if ( name && email && password && age ) {
                // Obtain the password in request body and cypher
                hashedPassword = bcrypt.hashSync(password, 8);
    
                // Object to create user
                let newUser: IUser = {
                    name,
                    email,
                    password: hashedPassword,
                    age,
                    katas: [],
                    solvedKatas: []
                }
    
                // Controller Instance to execute method
                const controller: AuthController = new AuthController;
    
                // Obtain Response
                const response: any = await controller.registerUser(newUser);
    
                // Send to the client the response
                return res.status(200).send(response);
    
            } else {
                return res.status(400).send({
                    message: 'CREATE REQUEST MISSING DATA: Please, provide an complete user Entity to create one'
                });
            }
            
        } catch (err) {
            return res.status(400).send({
                message: 'CREATE ERROR: Some of the data in create request is invalid'
            });
        }
    })


// http://localhost:8000/api/auth/login
authRouter.route('/login')
    .post(async (req: Request, res: Response) => {
        try {
            // Obtain data of user in request body
            const { email, password } = req?.body;

            if ( email && password ) {
                // Object to auth
                let auth: IAuth = {
                    email,
                    password
                }
                
                // Controller Instance to execute method
                const controller: AuthController = new AuthController;
        
                // Obtain Response
                const response: any = await controller.loginUser(auth);
        
                // Send to the client the response which includes the JWT to authorize request
                return res.status(200).send(response);
    
            } else {
                // Send to the client the response error
                return res.status(400).send({
                    message: `LOGIN REQUEST MISSING DATA: Please, provide an email and password`
                });
            }
            
        } catch (err) {
            return res.status(400).send({
                message: 'LOGIN ERROR: Some of the data in post request is invalid'
            });
        }
    })


export default authRouter;
