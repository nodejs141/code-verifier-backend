import express, { Express, Request, Response } from 'express';
import authRouter from './AuthRouter';
import helloRouter from './HelloRouter';
import katasRouter from './KatasRouter';
import usersRouter from './UsersRouter';

// Server instance
let routes: Express = express();

// Content type config
routes.use(express.urlencoded({ extended: true, limit: '50mb' }));
routes.use(express.json({ limit: '50mb' }))


// Activate for requests to http://localhost:8000/api
// Router instance
let rootRouter = express.Router();

// GET: http://localhost:8000/api/
rootRouter.get('/', (req: Request, res: Response) => {
    // Send message
    res.send('Hey, welcome to API-Restful Express + TS + Nodemon + Jest + Swagger + Mongoose')
});


// Redirections to Routes & Controllers
routes.use('/', rootRouter); // http://localhost:8000/api/
routes.use('/hello', helloRouter); // http://localhost:8000/api/hello
routes.use('/users', usersRouter); // http://localhost:8000/api/users
routes.use('/katas', katasRouter); // http://localhost:8000/api/katas
routes.use('/auth', authRouter); // http://localhost:8000/api/auth


export default routes;
