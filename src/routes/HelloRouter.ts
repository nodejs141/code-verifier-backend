import { HelloController } from '../controller/HelloController';
import { LogInfo } from '../utils/logger';
import express, { Request, Response } from 'express'
import { BasicResponse } from '../controller/types';

// Router from express
let helloRouter = express.Router();

// http://localhost:8000/api/hello/
helloRouter.route('/')
    // GET
    .get(async (req: Request, res: Response) => {
        // Obtain query param
        let name: any = req?.query?.name;
        LogInfo(`Query param: ${name}`);

        // Controller Instance to execute method
        const controller: HelloController = new HelloController;

        // Obtain Response
        const response: BasicResponse = await controller.getMessage(name);

        // Send to the client the response
        return res.status(200).send(response)
    })

export default helloRouter;
