import dotenv from 'dotenv'; // Environment Variables
import server from './src/server';
import { LogError, LogSuccess } from './src/utils/logger';


// Configuration the .env file
dotenv.config();

// Create Express APP
const port: string | number = process.env.PORT || 8000;

// Execute APP and listen request to PORT
server.listen(port, () => {
    LogSuccess(`[SERVER ON]: Running in http://localhost:${port}`)
});

// Control server error
server.on('error', (error) => {
    LogError(`[SERVER ERROR]: ${error}`)
});
